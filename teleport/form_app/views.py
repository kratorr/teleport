from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Questionnaire
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import QuestionnaireForm1, QuestionnaireForm2, QuestionnaireForm3
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.shortcuts import reverse
import os
from django.core.files.images import ImageFile
from django.core.files import File
from django.core.serializers.json import DjangoJSONEncoder
import json


file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'temp_dir'))


def all_questionnaire(request):
    questionnaire_list = Questionnaire.objects.get_queryset().order_by('id')
    paginator = Paginator(questionnaire_list, 10)
    page = request.GET.get('page')
    questionnaire_list = paginator.get_page(page)
    return render(request, 'form_app/list.html', {'questionnaire_list': questionnaire_list})


def index(request):
    return render(request, 'form_app/index.html')


def step1(request):
    initial = {'first_name': request.session.get('first_name', None),
               'last_name': request.session.get('last_name', None),
               'sex': request.session.get('sex', None)
               }
    form = QuestionnaireForm1(request.POST or None, initial=initial)
    if request.method == 'POST':
        if form.is_valid():
            request.session['first_name'] = form.cleaned_data['first_name']
            request.session['last_name'] = form.cleaned_data['last_name']
            request.session['birthday_date'] = json.dumps(
                form.cleaned_data['birthday_date'],
                sort_keys=True,
                indent=1,
                cls=DjangoJSONEncoder
            )
            request.session['sex'] = form.cleaned_data['sex']
            return HttpResponseRedirect('/form_app/step2/')
    return render(request, 'form_app/step_1.html', {'form': form})


def step2(request):
    initial = {'hair_color': request.session.get('hair_color', None),
               'weight': request.session.get('weight', None)
        }
    form = QuestionnaireForm2(request.POST or None, request.FILES, initial=initial)
    if request.method == 'POST':
        if form.is_valid():
            photo = request.FILES['photo']
            filename = file_storage.save(photo.name, photo)
            request.session['photo'] = filename
            request.session['hair_color'] = form.cleaned_data['hair_color']
            request.session['weight'] = form.cleaned_data['weight']
            return HttpResponseRedirect('/form_app/step3')
    return render(request, 'form_app/step_2.html', {'form': form})


def step3(request):
    form = QuestionnaireForm3(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            file_path = os.path.join(file_storage.path(request.session['photo']))
            file = open(file_path, 'rb')
            file_object = ImageFile(file)
            questionnaire = Questionnaire(
                first_name=request.session['first_name'],
                last_name=request.session['last_name'],
                sex=request.session['sex'],
                hair_color=request.session['hair_color'],
                weight=request.session['weight'],
                birthday_date=json.loads(request.session['birthday_date']),
                favorite_animal=form.cleaned_data['favorite_animal'],
                have_animal=form.cleaned_data['have_animal'],
            )
            questionnaire.save()
            questionnaire.photo.save(request.session['photo'], file_object)
            request.session.flush()
            return HttpResponseRedirect('/form_app/done')
    return render(request, 'form_app/step_3.html', {'form': form})


def done(request):
    return render(request, 'form_app/done.html')

