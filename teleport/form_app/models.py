from django.db import models



class Questionnaire(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    birthday_date = models.DateField()
    sex = models.CharField(max_length=200)
    photo = models.FileField(upload_to='photo')
    hair_color = models.CharField(max_length=200)
    weight = models.IntegerField()
    favorite_animal = models.CharField(max_length=200)
    have_animal = models.BooleanField()

    def __str__(self):
        return self.first_name
