from django.urls import path


from . import views

app_name = 'form_app'


urlpatterns = [
    path('', views.index, name='index'),
    path('all_questionnaire', views.all_questionnaire, name='all_questionnaire'),
    path('step1/', views.step1, name='step1_url'),
    path('step2/', views.step2, name='step2_url'),
    path('step3/', views.step3, name='step3_url'),
    path('done/', views.done, name='done_url')

]
