from django import forms


SEX_CHOICES = (
    ('', ''),
    ("мужчина", ("мужчина")),
    ("женщица", ("женщина"))
)

HAIR_CHOICES = (
    ('', ''),
    ("рыжий", ("рыжий")),
    ("черный", ("черный")),
    ("блонд", ("блонд"))
)

FAVORITE_ANIMALS = (
    ('', ''),
    ("кот", ("кот")),
    ("собака", ("собака")),
    ("попугай", ("попугай")),
    ("хомяк", ("хомяк"))
)

YEARS = [x for x in range(1920, 2019)]


class QuestionnaireForm1(forms.Form):
    first_name = forms.CharField(max_length=200, label="Имя", widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(max_length=200, label="Фамилия", widget=forms.TextInput(attrs={'class': 'form-control'}))
    birthday_date = forms.DateField(label="Дата рождения", widget=forms.SelectDateWidget(years=YEARS, attrs={'class':'form-control'}))
    sex = forms.ChoiceField(choices=SEX_CHOICES, label="Пол", widget=forms.Select(attrs={"class": "form-control"}))


class QuestionnaireForm2(forms.Form):
    photo = forms.FileField(label="Фото", widget=forms.ClearableFileInput(attrs={"class": " form-control-file"}))
    hair_color = forms.ChoiceField(choices=HAIR_CHOICES, label="Цвет волос", widget=forms.Select(attrs={"class": "form-control"}))
    weight = forms.IntegerField(label="Вес", widget=forms.NumberInput(attrs={"class": "form-control"}))


class QuestionnaireForm3(forms.Form):
    favorite_animal = forms.ChoiceField(choices=FAVORITE_ANIMALS, label="Любимое животное", widget=forms.Select(attrs={"class": "form-control"}))
    have_animal = forms.BooleanField(required=False, label="Есть домашнее животное")
